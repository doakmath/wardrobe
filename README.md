# Wardrobify

Team:

* Austin Hall - Hats
* Mathew Doak - Shoes

## Getting Started

**Make sure you have Docker, Git, and Node.js 18.2 or above**

1. Fork this repository

2. Clone the forked repository onto your local computer:
git clone <<https://gitlab.com/npcsloan/microservice-two-shot.git>>

3. Build and run the project using Docker with these commands:
```
docker volume create two-shot-pgdata
docker-compose build
docker-compose up
```

- After running these commands, make sure all of your Docker containers are running

- View the project in the browser: http://localhost:3000/

![Img](/images/MainPage.png)

## Design

Wardobify is made up of 3 microservices which interact with one another.

- **Wardrobe**
- **Hats**
- **Shoes**

![Img](/images/WardrobifyDiagram.png)

## Project Structure

Our Hats database stores the details of the individual hats, the Shoes database store the details of the individual shoes, and the Wardrobe database stores the locations/bins used by them. Our 3 microservices share a Postgres database container which contains 3 seperate databases. Our Hats and Shoes databases obtain information from the Wardrobe database using a Poller, which talks to the Wardrobe database to keep track of the locations/bins of the hats and shoes. This application allows a user to create and keep track of their inventory of shoes and hats.


## SHOES MICROSERVICE

INDEX:
    1. A Brief Overview
    2. Shoes API Endpoints
        (with input/output examples and brief explanation)
    3. Bins API Endpoints
        (with input/output examples and brief explanation)


1.
|---------------------------------|A BRIEF OVERVIEW|-------------------------------------------|

The Wardrobe domain contains the Bin and Location models.  The pollers in the Hats and Shoes API Microservices make an internal request on port localhost:8100 to update their 'VALUE OBJECTS', or VO models, which stands for Value Object.  This object contains the href to populate an instance on the Microservice.  The Microservice can then use the data to create, list, or delete instances of Hats and Shoes using the Location or Bin data.

The Shoes Microservice allows a user to create, list, or delete from the catalgoue that they create based on their inventory.  The Microservice shows all of data for the Shoe object, the sub-bin, the bin, the closet, and the bin size, which it gets from the Wardrobe Domain via a poller which makes a request every 60 seconds.

It would be effective for an individual with a lot of shoes, a small or large business that needed to track their inventory of shoes and hats, with a convenient and effective Frontend.

There are also several links located throughout the pages that allow for easier navigation and a better user experience.

From the Shoe list, an individual Shoe can be deleted by clicking the Delete button.

The View option doesn't work yet, however there is a link included for future developement, which will then link to a Detail View of an individual Shoe.

----OPTIONAL INVENTORY VIEW OPTION----

The Shoes List View includes an option to see the Inventory Path.  By selecting the Sub-Bin which contains the Shoe you are looking for which can be found in the List, it will then show the Sub-Bin, Bin, and Closet Name so you can locate it.  Also there is a link to a page that shows a table of that same data, however, on that page you have the ability to delete the Sub-Bins.

2.
|-----------------------------------|SHOES API ENDPOINTS|--------------------------------------|

GET 8080/api/shoes/
POST 8080/api/shoes/
DELETE 8080/api/shoes/1/


GET (LIST VIEW):

URL: http://localhost:8080/api/shoes/

    An http request of GET will return a list of shoe objects.  A single instance, or list with
    one value, is shown below:

OUTPUT EXAMPLE:
    {
        "shoes": [
            {
                "id": 19,
                "manufacturer": "Nike",
                "model_name": "Air",
                "color": "Blue",
                "picture_url": "https://www.kitchensanctuary.com/wp-content/uploads/2021/06/Simple-Green-Salad-with-Vinaigrette-tall-FS-1144.webp",
                "bin": {
                    "import_href": "/api/bins/13/"
                }
            },

POST:

URL: http://localhost:8080/api/shoes/

    An http request of POST will create a new instance of a Shoe.

INPUT Example:
    {
    "manufacturer": "Shoe2",
    "model_name": "Shoe2",
    "color": "Shoe2",
        "bin": "/api/bins/1/"
    }

OUTPUT EXAMPLE:
    {
        "id": 34,
        "manufacturer": "Shoe2",
        "model_name": "Shoe2",
        "color": "Shoe2",
        "picture_url": null,
        "bin": {
            "import_href": "/api/bins/1/"
        }
    }

DELETE:

URL: http://localhost:8080/api/shoes/1/

    A DELETE http request requires a unique identifier.  If deleted, a
    JsonResponse will return true, or false if it was not, or because of
    error handling, a unique message.

OUTPUT EXAMPLE:
    {
        "deleted": true
    }
    ---OR---
    {
        "message": "Does not exist"
    }


The GET (DETAIL VIEW) and PUT endpoints are not presently created on this App.


3.
|------------------------------|BINS ENDPOINTS--------------------------------------------------|
Bins Endpoints:

GET (LIST VIEW): http://localhost:8100/api/bins/
POST: http://localhost:8100/api/bins/
DELETE: http://localhost:8100/api/bins/1/
PUT: http://localhost:8100/api/bins/1/
GET (DETAIL VIEW): http://localhost:8100/api/bins/12/

GET (LIST VIEW)
An http of GET request made to /api/bins will return a list
of bin objects.A single instance, or list with one value,
is shown below:

OUTPUT EXAMPLE:
    {
        "bins": [
            {
                "href": "/api/bins/8/",
                "id": 8,
                "closet_name": "#1",
                "bin_number": 1,
                "bin_size": 15
            }

POST
An http request of POST will create a new instance of a bin.

INPUT EXAMPLE
{
	"closet_name": "#3",
	"bin_number": 2,
	"bin_size": "5"
}
OUTPUT EXAMPLE:
{
	"href": "/api/bins/13/",
	"id": 13,
	"closet_name": "#3",
	"bin_number": 2,
	"bin_size": "5"
}

DELETE
A DELETE http request requires a unique identifier in the request.
If deleted, a JsonResponse will return true, false, or because of
error handling, a unique message.

INPUT EXAMPLE:
{
	"closet_name": "closet1",
	"bin_number": 1,
	"bin_size": "12"
}
OUTPUT EXAMPLE:
{
	"deleted": false
}
-OR-
{
    "message": "improper deletion, likely PK or does not exist"
}


PUT
The PUT http request requires a unique identifier also, and a Json
input with the new values you want to amend on an existing bin object.

INPUT EXAMPLE:
{
	"closet_name": "closet1",
	"bin_number": 1,
	"bin_size": "12"
}
OUTPUT EXAMPLE:
{
	"href": "/api/bins/12/",
	"id": 12,
	"closet_name": "closet1",
	"bin_number": 1,
	"bin_size": "12"
}

GET (DETAIL VIEW)
This http request requires a unique identifier and returns one single
instance of a bin object.

OUTPUT EXAMPLE:
{
	"href": "/api/bins/12/",
	"id": 12,
	"closet_name": "closet1",
	"bin_number": 1,
	"bin_size": 12
}





## Hats microservice

1.
|------------------------------|Overview|--------------------------------------------------|

The Hats Microservice is used to create, list, or delete hats. It stores the fabric, style, color, and location of hats.
It uses a poller to get a list of locations from the Wardrobe Microservice and stores each of their hrefs in a
Location Value Object or LocationVO. This LocationVO is used as a reference to the actual location when creating a hat instance.

2.
|------------------------------|Hats API Requests|--------------------------------------------------|

List Hats:
    
    Method: GET

    URL: http://localhost:8090/api/hats/

Example Output:
```
{
	"hats": [
		{
			"id": 1,
			"fabric": "Cotton",
			"style": "Top Hat",
			"color": "Black",
			"location": {
				"import_href": "/api/locations/1/"
			},
			"picture_url": null
		},
		{
			"id": 6,
			"fabric": "Cotton",
			"style": "Baseball",
			"color": "Red",
			"location": {
				"import_href": "/api/locations/1/"
			},
			"picture_url": "https://media.rallyhouse.com/products/59004769-1.jpg"
		}
	]
}
```
Create Hat:

    Method: POST

    URL: http://localhost:8090/api/hats/

Example Input:
```
{
  "fabric": "Cotton",
  "style": "Baseball",
  "color": "Red",
	"location": "/api/locations/1/",
	"picture_url": "https://media.rallyhouse.com/products/59004769-1.jpg"
}
```
Example Output:
```
{
	"id": 5,
	"fabric": "Cotton",
	"style": "Baseball",
	"color": "Red",
	"location": {
		"import_href": "/api/locations/1/"
	},
	"picture_url": "https://media.rallyhouse.com/products/59004769-1.jpg"
}
```
Delete Hat:

    Method: DELETE

    URL: http://localhost:8090/api/hats/6/

Example Output:
```
{
	"deleted": true
}
```
2.
|------------------------------|Locations API Requests|--------------------------------------------------|

List Locations:
    
    Method: GET

    URL: http://localhost:8100/api/locations/

Example Output:
```
{
	"locations": [
		{
			"href": "/api/locations/3/",
			"id": 3,
			"closet_name": "Closet C",
			"section_number": 2,
			"shelf_number": 2
		},
		{
			"href": "/api/locations/4/",
			"id": 4,
			"closet_name": "Closet D",
			"section_number": 2,
			"shelf_number": 2
		},
		{
			"href": "/api/locations/5/",
			"id": 5,
			"closet_name": "Closet E",
			"section_number": 2,
			"shelf_number": 2
		}
	]
}
```
Create Location:
    
    Method: POST

    URL: http://localhost:8100/api/locations/

Example Input:
```
{
  "closet_name": "Closet B",
  "section_number": 2,
  "shelf_number": 2
}
```
Example Output:
```
{
	"href": "/api/locations/1/",
	"id": 1,
	"closet_name": "Closet B",
	"section_number": 2,
	"shelf_number": 2
}
```
Show Location:
    
    Method: GET

    URL: http://localhost:8100/api/locations/1/

Example Output:
```
{
	"href": "/api/locations/1/",
	"id": 1,
	"closet_name": "Closet B",
	"section_number": 2,
	"shelf_number": 2
}
```
Edit Location:
    
    Method: PUT

    URL: http://localhost:8100/api/locations/1/

Example Input:
```
{
  "closet_name": "Closet B",
  "section_number": 2,
  "shelf_number": 3
}
```
Example Output:
```
{
	"href": "/api/locations/1/",
	"id": 1,
	"closet_name": "Closet B",
	"section_number": 2,
	"shelf_number": 3
}
```
Delete Location:

    Method: DELETE

    URL: http://localhost:8100/api/locations/1/

Example Output:
```
{
	"id": null,
	"closet_name": "Closet B",
	"section_number": 2,
	"shelf_number": 2
}
```
